
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


function onL(){

	var name = getUrlVars()["name"].replace("_"," ");
	document.getElementById("nome").innerHTML = name;
	
	var tel = getUrlVars()["tel"];
	document.getElementById("tel").innerHTML += tel;
	
	var addr = getUrlVars()["addr"].replace(/%20/g," ");
	document.getElementById("addr").innerHTML += addr;
}

function getTable(file, idDiv){
	
	getHeadTab(file+'headTable.txt', idDiv);
	
	var arrDati = getArrDati().split(";");	
	
	document.getElementById(idDiv).innerHTML += "<tbody>";
	for(var i=0; i<arrDati.length; i++){
		var dati = arrDati[i].split(",");
		var urlData = 'bin/profile.html?name='+dati[1]+'_'+dati[0]+'&tel='+dati[2]+'&addr='+dati[3];
		
		document.getElementById(idDiv).innerHTML +='<tr><td>'+(i+1)+'</td><td>'+dati[0]+'</td><td>'+dati[1]+'</td>'+
		'<td><a href="'+urlData+'">vedi profilo..</a></td></tr>';
	}
	document.getElementById(idDiv).innerHTML += "</tbody>";
}

function getArrDati(){
	
	var file = 'lib/people.txt';
	var str;
	
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = 
		function(){
			if(this.readyState == 4 && this.status == 200){
				str = this.responseText;
				return str;
			}
			
		};
		
		xhttp.open("GET",
			file,
			false);
		xhttp.send();

		return str;
}

function getHeadTab(file, idDiv){
	
	var xhttp = new XMLHttpRequest();
				
		xhttp.onreadystatechange = 
		function(){
			if(this.readyState == 4 && this.status == 200){
				document.getElementById(idDiv).innerHTML += this.responseText;
			}
		};
		
		xhttp.open("GET",
			file,
			false);
		xhttp.send();
}